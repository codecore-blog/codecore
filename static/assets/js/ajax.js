// BLOG DETAIL AJAX REQUEST
// Comment AJAX
$(document).on('submit', '#commentForm', function (event) {
   event.preventDefault();
   $.ajax({
      type: 'POST',
      url: window.page_data.add_comment_url, 
      data: $(this).serialize(),
      dataType: 'json',
      success: function (response) {
         $('.comment-section').html(response['form']);
         $('#message_modal').modal({keyboard: false});
         // Comment Load Start
         $(".comment-list").slice(0, 4).show();
         $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".comment-list:hidden").slice(0, 4).slideDown();
            if($(".comment-list:hidden").length == 0) {
               $("#loadMore").hide();
               }
            });
            // Comment Load End  
           }
      }
   )
})

var csrf = $("input[name=csrfmiddlewaretoken]").val();
// LIKE AJAX
$(document).on('submit', '.like', function (event) {
   event.preventDefault();
   $.ajax({
      type: 'POST',
      url: window.page_data.like_url, 
      data: {csrfmiddlewaretoken:csrf},
      dataType: 'json',
      success: function (response) {
         if (response.redirect == true) {
            $('#like-section').html(response['form']);
            $('#message_modal').modal({keyboard: false});  
            setTimeout(function() {
               window.location.href = window.page_data.login_url;
            }, 5000);
         } else {
            $('#like-section').html(response['form']);
         }
           }
      }
   )
})

$(document).on("submit", ".delete_comment", function(e){
   $('.modal').removeClass('in');
   $('.modal').attr("aria-hidden","true");
   $('.modal').css("display", "none");
   $('.modal-backdrop').remove();
   $('body').removeClass('modal-open');
});

// Comment_DELETE AJAX
$(document).on('submit', '.delete_comment', function (event) {
   event.preventDefault();
   $.ajax({
      type: 'POST',
      url: window.page_data.delete_comment_url, 
      data: $(this).serialize(),
      dataType: 'json',
      success: function (response) {
            $('.comment-section').html(response['form']);
            $('#message_modal').modal({keyboard: false}); 
            // Comment Load Start
            $(".comment-list").slice(0, 4).show();
            $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".comment-list:hidden").slice(0, 4).slideDown();
            if($(".comment-list:hidden").length == 0) {
               $("#loadMore").hide();
               }
            });
            // Comment Load End  
           }
      }
   )
})
// LOAD_MORE AJAX

$(document).ready(function(){
  $(".comment-list").slice(0, 4).show();
  $("#loadMore").on("click", function(e){
    e.preventDefault();
    $(".comment-list:hidden").slice(0, 4).slideDown();
    if($(".comment-list:hidden").length == 0) {
      $("#loadMore").hide();
    }
  });  
})
