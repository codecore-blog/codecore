from django.urls import path
from django.contrib.auth import views as auth_views
from  . import views
from .views import UserEditView,PasswordChangeView

urlpatterns = [
    path("register",views.register, name='register'),
    path("login",views.login, name='login'),
    path("logout",views.logout, name='logout'),

    path("my_blog/",views.my_blog, name='my_blog'),
    path('edit_profile/',UserEditView.as_view(), name='edit_profile'),
    # path('password/',auth_views.PasswordChangeView.as_view(template_name='register/change_password.html')),
    path('password/',PasswordChangeView.as_view(template_name='register/change_password.html'),name='password'),
    
    # path('reset_password/',
    #     auth_views.PasswordResetView.as_view(),
    #     name="reset_password"),

    # path('reset_password_sent/', 
    #     auth_views.PasswordResetDoneView.as_view(), 
    #     name="password_reset_done"),

    # path('reset/<uidb64>/<token>/',
    #     auth_views.PasswordResetConfirmView.as_view(), 
    #     name="password_reset_confirm"),

    # path('reset_password_complete/', 
    #     auth_views.PasswordResetCompleteView.as_view(), 
    #     name="password_reset_complete"),

    # AJAX URLS
    path("ajax/delete_account/",views.delete_user, name="delete_account"),
    # AJAX URLS  
]
