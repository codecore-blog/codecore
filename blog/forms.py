from django import forms
from .models import Blog, Category, Comment

#From Category table getting all Category
choices = Category.objects.all().values_list('category','category')

choice_list = []

for item in choices:
    choice_list.append(item)

# Form for AddPost 
class BlogForm(forms.ModelForm):
    class Meta:
        model= Blog
        fields = ('title','description','content','author','category','img')

        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'description': forms.TextInput(attrs={'class':'form-control'}),
            'content': forms.TextInput(attrs={'class':'form-control'}),
            'author': forms.TextInput(attrs={'class':'form-control', 'value':'', 'id':'auth', 'type':'hidden'}),
            # 'author': forms.Select(attrs={'class':'form-control'}),
            'category': forms.Select(choices=choice_list,attrs={'class':'form-control'})
        }

# Form for EditPost 
class EditForm(forms.ModelForm):
    class Meta:
        model= Blog
        fields = ('title','description','content','category','img')

        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'description': forms.TextInput(attrs={'class':'form-control'}),
            'content': forms.TextInput(attrs={'class':'form-control'}),
            'category': forms.Select(choices=choice_list,attrs={'class':'form-control'})
        }

# class CommentForm(forms.ModelForm):
#     class Meta:
#         model= Comment
#         fields = ('name','body')

#         widgets = {
#             'name': forms.TextInput(attrs={'class':'form-control'}),
#             'body': forms.Textarea(attrs={'class':'form-control'}),
#         }