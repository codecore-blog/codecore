from django.shortcuts import render,redirect, get_object_or_404
from .models import Blog,Category,Comment
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin
from django.http import HttpResponseRedirect,Http404,JsonResponse
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from .forms import BlogForm, EditForm #,CommentForm
from django.core.paginator import Paginator
from django import template
from django.template.loader import render_to_string
# from django.db.models import Qs
from django.contrib.postgres.search import *

# Create your views here.

def LikeView(request, pk):
    blogs = get_object_or_404(Blog, id=pk)
    liked = False
    if request.is_ajax and request.method == "POST" and request.user.is_authenticated == True:

        if blogs.likes.filter(id=request.user.id).exists():
            blogs.likes.remove(request.user)
            liked = False
        else:
            blogs.likes.add(request.user)
            liked = True

        # liked = False
        if blogs.likes.filter(id=request.user.id).exists():
            liked = True

        total_likes = blogs.total_likes() 
        context = {
            'liked':liked,
            'total_likes':total_likes
        }
        html = render_to_string('blog/like_section.html',context,request=request)
        return JsonResponse({'form':html})

    else:
        total_likes = blogs.total_likes() 
        context = {
            'liked':liked,
            'total_likes':total_likes
        }
        messages.error(request, 'You must be logged in to Like the Post')
        html = render_to_string('blog/like_section.html',context,request=request)
        return JsonResponse({'form':html,'redirect':True},status=200)


class HomeView(ListView):
    model = Blog
    template_name = 'blog/blog.html'

    def get_context_data(self, **kwargs):
        cat_menu = Category.objects.all()
        context = super(HomeView, self).get_context_data(**kwargs) 
        context["cat_menu"] = cat_menu 
        recent_blog = Blog.objects.all()[0:3]
        context["recent_blog"] = recent_blog 

        queryset_list =  Blog.objects.all()
        query = self.request.GET.get("q")

        if query:
            # queryset_list = queryset_list.filter(
            #     Q(title__text__search =query) |
            #     Q(description__icontains = query) |
            #     Q(author__username__icontains = query) |
            #     Q(content__icontains = query) 
            # ).distinct()
            queryset_list = Blog.objects.annotate(
                search = SearchVector('title','description','author','content')
                ).filter(search = query).distinct()
   
        paginator = Paginator(queryset_list, 4) # Show 25 contacts per page.

        page_number = self.request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj

        return context
    # def paginated_queryset(self, queryset, page_size):
    #     #Pagination Start --
    #     try:
    #         return super(HomeView, self).paginated_queryset(self, queryset_list, page_size)
    #     except:
    #         self.kwargs['page'] = 1
    #         return super(HomeView, self).paginated_queryset(self, queryset_list, page_size)
    #     #Pagination End --

class ArticleDetailView(DetailView,MultipleObjectMixin):
    model = Blog
    template_name = 'blog/blog-detail.html'

    def get_context_data(self, **kwargs ):
        cat_menu = Category.objects.all()
        comments_count = Comment.objects.filter(post=self.kwargs['pk']).count()
        parent_comments = Comment.objects.filter(post=self.kwargs['pk'], parent=None)
        recent_blog = Blog.objects.all()[0:3]
        
        context = super(ArticleDetailView, self).get_context_data(object_list='',**kwargs) #object_list=parent_comments,

        stuff = get_object_or_404(Blog, id=self.kwargs['pk'])
        total_likes = stuff.total_likes() 

        liked = False
        if stuff.likes.filter(id=self.request.user.id).exists():
            liked = True

        context["cat_menu"] = cat_menu 
        context["recent_blog"] = recent_blog 
        context["total_likes"] = total_likes
        context["liked"] = liked
        context["comments_count"] = comments_count
        context["parent_comments"] = parent_comments
        return context
        # def paginated_queryset(self, queryset, page_size):
        #     #Pagination Start --
        #     try:
        #         return super(MultipleObjectMixin, self).paginated_queryset(self, queryset, page_size)
        #     except:
        #         self.kwargs['page'] = 1
        #         return super(MultipleObjectMixin, self).paginated_queryset(self, queryset, page_size)
        #     #Pagination End --

def CategoryView(request, cats):
    category_posts = Blog.objects.filter(category = cats.replace('-', ' '))
    paginator = Paginator(category_posts, 5) # Show 25 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cat_menu = Category.objects.all()
    recent_blog = Blog.objects.all()[0:3]
    if category_posts:
        return render(request, 'blog/category.html', {'cats':cats.title().replace('-', ' '), 'page_obj':page_obj, 'cat_menu':cat_menu, 'recent_blog':recent_blog})
    else:
        return render(request, 'main/404.html')

def DateView(request, post_date):
    date_posts = Blog.objects.filter(date__date = post_date)
    cat_menu = Category.objects.all()
    recent_blog = Blog.objects.all()[0:3]
    if date_posts:
        return render(request, 'blog/date_post.html', {'date':post_date.title(), 'date_posts':date_posts, 'cat_menu':cat_menu, 'recent_blog':recent_blog})
    else:
        return render(request, 'main/404.html')

def AuthorView(request, author):
    author_posts = Blog.objects.filter(author__username = author)
    cat_menu = Category.objects.all()
    recent_blog = Blog.objects.all()[0:3]
    if author_posts:
        return render(request, 'blog/author_post.html', {'author':author, 'author_posts':author_posts, 'cat_menu':cat_menu, 'recent_blog':recent_blog})
    else:
        return render(request, 'main/404.html')

class AddPostView(CreateView):
    model = Blog
    form_class = BlogForm
    template_name = 'blog/add_post.html'
    # fields = '__all__'
    # fields = ('title','description','content','author','category','likes','comments')

# class AddCommentView(CreateView):
#     model = Comment
#     form_class = CommentForm
#     template_name = 'blog/blog-detail.html'

#     def form_valid(self,form):
#         form.instance.blog_id = self.kwargs['pk']
#         return super().form_valid(form)
#     success_url = reverse_lazy('blog')

def AddCommentView(request, pk):
    # Getting Post ID 
    article = get_object_or_404(Blog, id=pk)
    # Takin value from the html file 
    name = request.POST.get('name')
    email = request.POST.get('email')
    body = request.POST.get('body')
    website = request.POST.get('website')
    parentsno = request.POST.get('parentsno')

    blog = get_object_or_404(Blog, id=pk)
    comments_count = Comment.objects.filter(post=pk).count()
    parent_comments = Comment.objects.filter(post=pk, parent=None)

    # paginator = Paginator(parent_comments, 5) # Show 25 contacts per page.

    # page_number = request.GET.get('page')
    # page_obj = paginator.get_page(page_number)
    
    context = {
        'comments_count':comments_count,
        'parent_comments':parent_comments,
        'blog':blog
    }

    if request.is_ajax and request.method == "POST" and request.user.is_authenticated == True:
        try:
            if User.username == name:
                if parentsno == "":
                    newComment = Comment(name=name,body=body,post=article)
                    messages.success(request, 'Comment Posted Successfuly')
                else:
                    parent = Comment.objects.get(id=parentsno)
                    newComment = Comment(name=name,body=body,post=article,parent=parent)
                    messages.success(request, 'Reply Posted Successfuly')
            else:
                raise ValueError('stuff is not in content')
        except:
            messages.error(request, 'Something went wrong')
            html = render_to_string('blog/comments.html',context,request=request)
            return JsonResponse({'form':html},status=200)

        #  Creating the object for the 
        newComment.article = article
        newComment.save()   

        blog = get_object_or_404(Blog, id=pk)
        comments_count = Comment.objects.filter(post=pk).count()
        parent_comments = Comment.objects.filter(post=pk, parent=None)

        context = {
            'comments_count':comments_count,
            'parent_comments':parent_comments,
            'blog':blog
        } 

        html = render_to_string('blog/comments.html',context,request=request)
        return JsonResponse({'form':html},status=200)
        # return redirect(reverse("blog-detail",kwargs={"pk":pk}))
    else:
        messages.error(request, 'You must be logged in to Post a comment')
        html = render_to_string('blog/comments.html',context,request=request)
        return JsonResponse({'form':html},status=200)

class UpdatePostView(UpdateView):
    model = Blog
    form_class = EditForm
    template_name = 'blog/update_post.html'

class DeletePostView(DeleteView):
    model = Blog
    template_name = 'blog/delete_post.html'
    success_url = reverse_lazy('blog')

class AddCategoryView(CreateView):
    model = Category
    template_name = 'blog/add_category.html'
    fields = '__all__'

def Delete_Comment(request,pk):
    comment_id = request.POST.get('comment_id')
    parentsno = request.POST.get('parentsno')

    try:
        comment = get_object_or_404(Comment,post=pk,id=comment_id,name=request.user)
    except:
        raise Http404

    if request.is_ajax and request.method == "POST" and request.user.is_authenticated == True:
        try:
            comment.delete()
            messages.success(request, 'You have successfully deleted the comment')
            
            blog = get_object_or_404(Blog, id=pk)
            comments_count = Comment.objects.filter(post=pk).count()
            parent_comments = Comment.objects.filter(post=pk, parent=None)
            context = {
                'comments_count':comments_count,
                'parent_comments':parent_comments,
                'blog':blog
            }

            html = render_to_string('blog/comments.html',context,request=request)
            return JsonResponse({'form':html},status=200)

        except:
            blog = get_object_or_404(Blog, id=pk)
            comments_count = Comment.objects.filter(post=pk).count()
            parent_comments = Comment.objects.filter(post=pk, parent=None)
            context = {
                'comments_count':comments_count,
                'parent_comments':parent_comments,
                'blog':blog
            }
            messages.warning(request, 'The comment could not be deleted.')
            html = render_to_string('blog/comments.html',context,request=request)
            return JsonResponse({'form':html},status=200)
    else:
        raise Http404


    