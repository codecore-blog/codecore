from django.urls import path,include
from  . import views
from .views import HomeView, ArticleDetailView, AuthorView, DateView, CategoryView, LikeView, AddPostView, UpdatePostView, DeletePostView, AddCategoryView, AddCommentView
from django.conf.urls import url
from django.contrib.admin.views.decorators import staff_member_required

urlpatterns = [
    path('',HomeView.as_view(), name="blog"),
    path('register/',include('register.urls')),
    path('article/<int:pk>/',ArticleDetailView.as_view(), name='blog-detail'),
    path('category/<str:cats>/',CategoryView, name='category'),
    path('date/<str:post_date>/',DateView, name='date'),
    path('author/<str:author>/',AuthorView, name='author'),
    # path('like/<int:pk>',LikeView, name='like_post'),
    
    # ADMIN ONLY
    path('add_post/',staff_member_required(AddPostView.as_view(),login_url='/'), name='add_post'),
    path('add_category/',staff_member_required(AddCategoryView.as_view(),login_url='/'), name='add_category'),
    path('article/edit/<int:pk>/',staff_member_required(UpdatePostView.as_view(),login_url='/'), name='update_post'),
    path('article/<int:pk>/delete/',staff_member_required(DeletePostView.as_view(),login_url='/'), name='delete_post'),
    # ADMIN ONLY
    
    # AJAX URLS
    path("ajax/add_comment/<int:pk>/",views.AddCommentView, name="add_comment"),
    path("ajax/like/<int:pk>/",views.LikeView, name="like_post"),
    path("ajax/delete_comment/<int:pk>/",views.Delete_Comment, name="delete_comment"),
    # AJAX URLS   
    # url(r'^ajax/like/<int:pk>$', views.LikeView, name = "like_post"),
]
