from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.urls import reverse

#Create your models here.

class Category(models.Model):
    category = models.CharField(max_length=100)

    def __str__(self):
        return self.category

    def get_absolute_url(self):
        return reverse("blog")
        
class Blog(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    img = models.ImageField(null=True, blank=True, upload_to='pics/')
    content = RichTextField(blank=True, null=True)
    # content = models.TextField()
    comments = models.IntegerField(default='0')
    likes = models.ManyToManyField(User, related_name='blog_posts')
    author = models.ForeignKey(User,on_delete = models.CASCADE)
    category = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)

    def total_likes(self):
        return self.likes.count()

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog")

class Comment(models.Model):
    post = models.ForeignKey(Blog, related_name="comment", on_delete = models.CASCADE)
    name = models.CharField(max_length=255)
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    parent = models.ForeignKey('self', related_name="replies", null=True, blank=True, on_delete= models.CASCADE)
        
    class Meta:
        ordering = ['-date_added']

    def __str__(self):
        return '%s - %s' % (self.post.title, self.name)
