import threading
import mailchimp
from django.conf import settings
from mailchimp import Mailchimp

class SendSubscribeMail(object):
	def __init__(self, email):
		self.email = email
		thread = threading.Thread(target=self.run, args=())
		thread.daemon = True                     
		thread.start()                                 

	def run(self):
		API_KEY = settings.MAILCHIMP_API_KEY
		LIST_ID = settings.MAILCHIMP_SUBSCRIBE_LIST_ID
		api = mailchimp.Mailchimp(API_KEY)
		try:
			api.lists.subscribe(LIST_ID, {'email': self.email}, double_optin=False)
		except:
			return False

	# def send_mail(campaign_id, client=client):      
    #     try:
	# 		mailchimp = Mailchimp(api_key)
	# 		mailchimp.campaigns.send(campaign_id)
    #         client.campaigns.actions.send(campaign_id = campaign_id)
    #     except Exception as error:
    #         print(error)