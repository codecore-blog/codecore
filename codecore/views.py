from django.shortcuts import render,redirect
from . models import Course
from blog.models import Blog
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.core.mail import send_mail

# MAILCHIMP
from django.http import HttpResponse, JsonResponse
from .models import Subscribe
from .utils import SendSubscribeMail
# MAILCHIMP

# Create your views here.

def index(request):
    courses = Course.objects.all()[0:3]
    blogs = Blog.objects.all()[0:2]
    # courses = Course.objects.all().order_by('-date')
    return render(request, 'main/index.html', {'courses' : courses , 'blogs' : blogs}) 

def about(request):
    return render(request, 'main/about.html') 
    
def contact(request):
    if request.method == 'POST':
        message_name = request.POST['message-name']
        message_email = request.POST['message-email']
        message_subject = request.POST['message-subject']
        message = request.POST['message']
        
        # Sending mails
        send_mail(
            # message_name,# name
            message_subject, # subject
            message, # message
            message_email, # from email
            ['codecore@gmail.com'], # to email
        )
        return render(request, 'main/contact.html') 
    else:
        return render(request, 'main/contact.html') 

# MAILCHIMP
def subscribe(request):
    if request.method == 'POST':
        email = request.POST['email_id']
        email_qs = Subscribe.objects.filter(email_id = email)
        if email_qs.exists():
            data = {"status" : "404"}
            messages.error(request, 'This Email is already been subscribed!')
            return JsonResponse(data)
        else:
            Subscribe.objects.create(email_id = email)
            SendSubscribeMail(email) # Send the Mail, Class available in utils.py
            messages.success(request, 'Thank you for Subscribing! Please Check your Email to Confirm the Subscription')
            
    return HttpResponse("/")

