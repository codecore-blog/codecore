from django.db import models
from django.utils import timezone
# Create your models here.

class Course(models.Model):

    title = models.CharField(max_length=100)
    img = models.ImageField(upload_to='pics')
    rating = models.IntegerField()
    review = models.IntegerField()
    offer = models.BooleanField(default=False)
    members = models.IntegerField()
    likes = models.IntegerField()
    price = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.title

class Subscribe(models.Model):
    email_id = models.EmailField(null = True, blank = True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title