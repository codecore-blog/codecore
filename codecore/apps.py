from django.apps import AppConfig


class CodecoreConfig(AppConfig):
    name = 'codecore'
